%define find_value 0
%macro colon 2
    %ifid %2
        %2: dq find_value
    %else
        %error "Second argument is not id"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "First argument is not string"
    %endif
    %define find_value %2
%endmacro