%include "lib.inc"

global find_word
global get_value

find_word:
    push r12
    mov r12, rdi
    push r13
    mov r13, rsi
    .loop:

        mov rdi, r12
        lea rsi, [r13+8]
        call string_equals
        test rax, rax
        jnz .found

        mov r13, [r13]
        test r13, r13
        jnz .loop
    .not_found:
        xor rax, rax
        jmp .end
    .found:
        mov rax, r13
    .end:
        pop r13
        pop r12
    ret

get_value:
    lea rdi, [rdi+8]
    push rdi
    call string_length
    pop rdi
    add rax, rdi
    inc rax
    ret