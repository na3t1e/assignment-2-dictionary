%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%include "dict.inc"



section .bss
    buff: resb 256

section .rodata
not_found_message: db "Key's not found", 0
out_of_size_message: db "Key doesn't fit buffer size", 0

section .text
global _start

_start:
    mov rdi, buff
    mov rsi, 256
    call read_word
    test rax, rax
    jz .out_of_size
    mov rdi, rax
    mov rsi, DICT
    call find_word
    test rax, rax
    jz .not_found
    mov rdi, rax
    call get_value
    mov rdi, rax
    call print_string
    call print_newline
    call exit
.out_of_size:
    mov rdi, out_of_size_message
    call print_err
    call print_newline
    call exit
.not_found:
    mov rdi, not_found_message
    call print_err
    call print_newline
    call exit