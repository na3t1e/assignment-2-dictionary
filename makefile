ASM = nasm
ASMFLAGS = -f elf64
LD = ld
OBJECTS = lib.o main.o dict.o
TARGET = program

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@

$(TARGET): $(OBJECTS)
	$(LD) -o $@ $^

.PHONY: test clean

test: test.py
	$(PYTHON3) $<
clean:
	rm -f *.o
